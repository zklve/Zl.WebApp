# Zl.WebApp

#### 介绍
Zl.WebApp说明：<br>
  -使用ok-admin+ASP.NET CORE3.1 搭建的通用权限(RBAC)后台管理系统<br>
  -支持定时任务<br>
  -支持MYSQL(支持),SQLSERVER(支持)，ORACLE切换(未实现)<br>
  -支持分布式缓存<br>
  -支持Autofac属性注入<br>
  -快速开发,简单封装<br>
.net core 项目地址:[https://gitee.com/Zl819058637/Zl.WebApp.git](https://gitee.com/Zl819058637/Zl.WebApp.git) 支持Quartz<br>
.net framework项目地址：[https://gitee.com/Liu_Cabbage/FineAdmin.Mvc.git](https://gitee.com/Liu_Cabbage/FineAdmin.Mvc.git)<br>

#### 技术选型
ASP.NET CORE3.1 Dapper DapperExtensions Autofac Redis Nlog Quartz

#### 菜单结构
- 系统管理
1. 机构管理
2. 字典分类
3. 数据字典
4. 用户管理
5. 角色管理
6. 系统菜单
7. 按钮管理
- 系统安全
1. 登录日志
2. 定时任务
- 系统设置
1. 网站设置
2. 开发设置
3. 邮件设置

#### 项目截图
![输入图片说明](img/1.png "1.png")
![输入图片说明](img/2.png "2.png")
![输入图片说明](img/3.png "3.png")
![输入图片说明](img/4.png "4.png")
![输入图片说明](img/5.png "5.png")
![输入图片说明](img/6.png "6.png")
![输入图片说明](img/7.png "7.png")
![输入图片说明](img/8.png "8.png")



#### 使用说明

1. 先创建数据库，然后执行Zl.Web/database目录下面的sql即可<br>
2. 修改数据库类型(目前支持SQLSERVER 和 MYSQL)【注意:活动Debug，Debug,Release几种属性模式都要配置】
![输入图片说明](img/9.png "9.png")
3. 要使用redis同学,注释Startup.cs中 services.AddDistributedMemoryCache   取消注释  services.AddStackExchangeRedisCache
4. 修改Zl.Web/appsettings.json
5. 账号：admin<br>
6. 密码：123456<br>
7. 设置数据库忽略大小写配置
8. 发布到linux出现不能正常显示验证码问题参考文章（https://blog.csdn.net/weixin_38169206/article/details/101093263）

#### 特别感谢

1. [Layui](https://www.layui.com)
2. [ok-admin](https://gitee.com/bobi1234/ok-admin)
3. [FineAdmin.Mvc](https://gitee.com/Liu_Cabbage/FineAdmin.Mvc.git)

#### 开源协议

[GPL-3.0](https://gitee.com/Liu_Cabbage/FineAdmin.Mvc/blob/master/LICENSE)



#### 作者QQ
819058637