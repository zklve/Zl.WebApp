﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Quartz;
namespace Zl.Test.QuartzJob1
{
    public class ExampleJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Console.Out.WriteLineAsync(this.GetType().FullName);
        }
    }
}
