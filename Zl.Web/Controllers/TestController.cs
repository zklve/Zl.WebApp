﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Zl.Model;
using Zl.Common;
using System.IO;
using Zl.IService;
using Microsoft.Extensions.Caching.Memory;

namespace Zl.Web.Controllers
{
    public class TestController : Controller
    {
        public ITestService TestService { get; set; }
        public IMemoryCache MemoryCache { get; set; }

        public ActionResult Index()
        {
            TestService.Run();
            MemoryCache.Set("1221", "sdasda");
            Console.WriteLine(MemoryCache.Get("1221"));
            return View();
        }

    }
}