﻿
using Zl.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Zl.Web
{
    public class HandlerLoginAttribute : Attribute, IActionFilter
    {
        public bool Ignore = true;

        public HandlerLoginAttribute(bool ignore = true)
        {
            Ignore = ignore;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (Ignore == false)
            {
                return;
            }
            if (new OperatorProvider(context.HttpContext).GetCurrent() == null)
            {
                context.HttpContext.Response.Redirect("/Login");
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            //if (context.Result?.GetType() == typeof(JsonResult))
            //{
            //    JsonResult result = context.Result as JsonResult;
            //    JObject valueObject = (JObject)(result.Value);
            //    valueObject["xb"] = "xxxx";
            //    result.Value = valueObject;
            //    context.Result = result;
            //    string yy = "";
            //}
        }
    }
}