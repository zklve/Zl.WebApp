USE [master]
GO
/****** Object:  Database [zlweb]    Script Date: 2020/2/3 12:09:10 ******/
CREATE DATABASE [zlweb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'zlweb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\zlweb.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'zlweb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\zlweb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [zlweb] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [zlweb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [zlweb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [zlweb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [zlweb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [zlweb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [zlweb] SET ARITHABORT OFF 
GO
ALTER DATABASE [zlweb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [zlweb] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [zlweb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [zlweb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [zlweb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [zlweb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [zlweb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [zlweb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [zlweb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [zlweb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [zlweb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [zlweb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [zlweb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [zlweb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [zlweb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [zlweb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [zlweb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [zlweb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [zlweb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [zlweb] SET  MULTI_USER 
GO
ALTER DATABASE [zlweb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [zlweb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [zlweb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [zlweb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [zlweb]
GO
/****** Object:  Table [dbo].[button]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[button](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EnCode] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[Location] [int] NULL,
	[ClassName] [nvarchar](50) NULL,
	[Icon] [nvarchar](50) NULL,
	[SortCode] [int] NULL,
	[CreateTime] [datetime2](7) NULL,
	[CreateUserId] [int] NULL,
	[UpdateTime] [datetime2](7) NULL,
	[UpdateUserId] [int] NULL,
 CONSTRAINT [PK__button__3214EC07551CF84B] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[donation]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[donation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Price] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NULL,
	[Detail] [nvarchar](100) NULL,
	[CreateTime] [datetime2](7) NULL,
 CONSTRAINT [PK__donation__3214EC072CF97C71] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[items]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[items](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[EnCode] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[SortCode] [int] NULL,
	[CreateTime] [datetime2](7) NULL,
	[CreateUserId] [int] NULL,
	[UpdateTime] [datetime2](7) NULL,
	[UpdateUserId] [int] NULL,
 CONSTRAINT [PK__items__3214EC07E67F83C8] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[itemsdetail]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[itemsdetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [int] NULL,
	[ItemCode] [nvarchar](50) NULL,
	[ItemName] [nvarchar](50) NULL,
	[SortCode] [int] NULL,
	[CreateTime] [datetime2](7) NULL,
	[CreateUserId] [int] NULL,
	[UpdateTime] [datetime2](7) NULL,
	[UpdateUserId] [int] NULL,
 CONSTRAINT [PK__itemsdet__3214EC0723BBB62C] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[logonlog]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[logonlog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogType] [nvarchar](50) NULL,
	[Account] [nvarchar](50) NULL,
	[RealName] [nvarchar](50) NULL,
	[Description] [nvarchar](200) NULL,
	[IPAddress] [nvarchar](50) NULL,
	[IPAddressName] [nvarchar](50) NULL,
	[CreateTime] [datetime2](7) NULL,
 CONSTRAINT [PK__logonlog__3214EC07FFAB033C] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[module]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[module](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[FullName] [nvarchar](50) NULL,
	[FontFamily] [nvarchar](50) NULL,
	[Icon] [nvarchar](50) NULL,
	[UrlAddress] [nvarchar](100) NULL,
	[SortCode] [int] NULL,
	[CreateTime] [datetime2](7) NULL,
	[CreateUserId] [int] NULL,
	[UpdateTime] [datetime2](7) NULL,
	[UpdateUserId] [int] NULL,
 CONSTRAINT [PK__module__3214EC07FA744E1A] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[organize]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[organize](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[EnCode] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[CategoryId] [int] NULL,
	[SortCode] [int] NULL,
	[CreateTime] [datetime2](7) NULL,
	[CreateUserId] [int] NULL,
	[UpdateTime] [datetime2](7) NULL,
	[UpdateUserId] [int] NULL,
 CONSTRAINT [PK__organize__3214EC0736AA3D3C] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[role]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EnCode] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[TypeClass] [int] NULL,
	[SortCode] [int] NULL,
	[CreateTime] [datetime2](7) NULL,
	[CreateUserId] [int] NULL,
	[UpdateTime] [datetime2](7) NULL,
	[UpdateUserId] [int] NULL,
 CONSTRAINT [PK__role__3214EC074D46C6CD] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[roleauthorize]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roleauthorize](
	[RoleId] [nchar](10) NOT NULL,
	[ModuleId] [nchar](10) NOT NULL,
	[ButtonId] [nchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[ModuleId] ASC,
	[ButtonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taskschedule]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taskschedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[JobGroup] [nvarchar](255) NOT NULL,
	[JobName] [nvarchar](255) NOT NULL,
	[CronExpress] [nvarchar](255) NOT NULL,
	[StarRunTime] [datetime2](7) NULL,
	[EndRunTime] [datetime2](7) NULL,
	[CreateTime] [datetime2](7) NULL,
	[State] [int] NOT NULL,
 CONSTRAINT [PK__tasksche__3214EC071A928548] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users]    Script Date: 2020/2/3 12:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Account] [nvarchar](50) NULL,
	[UserPassword] [nvarchar](50) NULL,
	[RealName] [nvarchar](50) NULL,
	[HeadIcon] [nvarchar](50) NULL,
	[Gender] [smallint] NULL,
	[Birthday] [datetime2](7) NULL,
	[MobilePhone] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[WeChat] [nvarchar](50) NULL,
	[DepartmentId] [int] NULL,
	[RoleId] [int] NULL,
	[EnabledMark] [smallint] NULL,
	[CreateTime] [datetime2](7) NULL,
	[CreateUserId] [int] NULL,
	[UpdateTime] [datetime2](7) NULL,
	[UpdateUserId] [int] NULL,
 CONSTRAINT [PK__users__3214EC071B97522D] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[button] ON 

INSERT [dbo].[button] ([Id], [EnCode], [FullName], [Location], [ClassName], [Icon], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (1, N'add', N'添加', 1, N'', N'&#xe6d9;', 1, CAST(0x0780083FA98950400B AS DateTime2), 1, CAST(0x078062A1AB8950400B AS DateTime2), 1)
INSERT [dbo].[button] ([Id], [EnCode], [FullName], [Location], [ClassName], [Icon], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (2, N'del', N'删除', 0, N'layui-btn-danger', N'&#xe659;', 3, CAST(0x07806C87338A50400B AS DateTime2), 1, CAST(0x07003051358A50400B AS DateTime2), 1)
INSERT [dbo].[button] ([Id], [EnCode], [FullName], [Location], [ClassName], [Icon], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (3, N'edit', N'修改', 0, N'', N'&#xe761;', 2, CAST(0x0700843B608A50400B AS DateTime2), 1, CAST(0x0700DE9D628A50400B AS DateTime2), 1)
INSERT [dbo].[button] ([Id], [EnCode], [FullName], [Location], [ClassName], [Icon], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (4, N'batchDel', N'批量删除', 1, N'layui-btn-danger', N'&#xe6b4;', 4, CAST(0x07007A82098B50400B AS DateTime2), 1, CAST(0x07803D4C0B8B50400B AS DateTime2), 1)
INSERT [dbo].[button] ([Id], [EnCode], [FullName], [Location], [ClassName], [Icon], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (5, N'pwdReset', N'重置密码', 0, N'layui-btn-warm', N'&#xe6a4;', 5, CAST(0x07803B4B184F60400B AS DateTime2), 1, CAST(0x070086A81D4F60400B AS DateTime2), 1)
INSERT [dbo].[button] ([Id], [EnCode], [FullName], [Location], [ClassName], [Icon], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (6, N'assign', N'分配权限', 0, N'layui-btn-normal', N'&#xe73a;', 6, CAST(0x0780E4A1456362400B AS DateTime2), 1, CAST(0x07803E04486362400B AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[button] OFF
SET IDENTITY_INSERT [dbo].[donation] ON 

INSERT [dbo].[donation] ([Id], [Name], [Price], [Source], [Detail], [CreateTime]) VALUES (1, N'1221', N'21', N'21', N'21', CAST(0x078076C461A1A6400B AS DateTime2))
SET IDENTITY_INSERT [dbo].[donation] OFF
SET IDENTITY_INSERT [dbo].[items] ON 

INSERT [dbo].[items] ([Id], [ParentId], [EnCode], [FullName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (1, 0, N'Sys_Items', N'通用字典', 0, CAST(0x0780D7FFD1623F400B AS DateTime2), 1, CAST(0x078036B729956C400B AS DateTime2), 1)
INSERT [dbo].[items] ([Id], [ParentId], [EnCode], [FullName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (2, 1, N'OrganizeCategory', N'机构分类', 1, CAST(0x07002E39E7833F400B AS DateTime2), 1, CAST(0x07801E34EA833F400B AS DateTime2), 1)
INSERT [dbo].[items] ([Id], [ParentId], [EnCode], [FullName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (3, 1, N'RoleType', N'角色类型', 2, CAST(0x0700AF5413843F400B AS DateTime2), 1, CAST(0x070009B715843F400B AS DateTime2), 1)
INSERT [dbo].[items] ([Id], [ParentId], [EnCode], [FullName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (4, 1, N'Certificate', N'证件名称', 3, CAST(0x0700D84495843F400B AS DateTime2), 1, CAST(0x070032A797843F400B AS DateTime2), 1)
INSERT [dbo].[items] ([Id], [ParentId], [EnCode], [FullName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (5, 1, N'Education', N'学历', 4, CAST(0x078000DCB4843F400B AS DateTime2), 1, CAST(0x0700C4A5B6843F400B AS DateTime2), 1)
INSERT [dbo].[items] ([Id], [ParentId], [EnCode], [FullName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (6, 1, N'Marry', N'婚姻', 5, CAST(0x0780FA63DD843F400B AS DateTime2), 1, CAST(0x078054C6DF843F400B AS DateTime2), 1)
INSERT [dbo].[items] ([Id], [ParentId], [EnCode], [FullName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (7, 1, N'Birth', N'生育', 6, CAST(0x0780F59DCA853F400B AS DateTime2), 1, CAST(0x0780BF0326559F400B AS DateTime2), 1)
INSERT [dbo].[items] ([Id], [ParentId], [EnCode], [FullName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (8, 1, N'Nation', N'民族', 7, CAST(0x07803B61EE853F400B AS DateTime2), 1, CAST(0x07007897EC853F400B AS DateTime2), 1)
INSERT [dbo].[items] ([Id], [ParentId], [EnCode], [FullName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (9, 1, N'Gender', N'性别', 8, CAST(0x0700C116FC853F400B AS DateTime2), 1, CAST(0x0780278D2F559F400B AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[items] OFF
SET IDENTITY_INSERT [dbo].[itemsdetail] ON 

INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (1, 2, N'Company', N'公司', 1, CAST(0x0780FABD3F873F400B AS DateTime2), 1, CAST(0x0780815143873F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (2, 2, N'Department', N'部门', 2, CAST(0x0700F36772873F400B AS DateTime2), 1, CAST(0x0780B63174873F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (3, 2, N'WorkGroup', N'小组', 3, CAST(0x07802CE284873F400B AS DateTime2), 1, CAST(0x0780591386873F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (4, 3, N'Sys_Role', N'系统角色', 1, CAST(0x0700EF26F3873F400B AS DateTime2), 1, CAST(0x07004989F5873F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (5, 3, N'Bus_Role', N'业务角色', 2, CAST(0x070002413E883F400B AS DateTime2), 1, CAST(0x07003D9946883F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (6, 3, N'Other', N'其他角色', 3, CAST(0x0780FDA65C883F400B AS DateTime2), 1, CAST(0x07802AD85D883F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (7, 4, N'Id_Card', N'身份证', 1, CAST(0x07809A1097883F400B AS DateTime2), 1, CAST(0x0780F47299883F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (8, 4, N'Other', N'其他', 2, CAST(0x07003958C7883F400B AS DateTime2), 1, CAST(0x07006689C8883F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (9, 5, N'Junior College', N'大专', 1, CAST(0x070004A5C7893F400B AS DateTime2), 1, CAST(0x0780C76EC9893F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (10, 5, N'Undergraduate', N'本科', 2, CAST(0x078010EED8893F400B AS DateTime2), 1, CAST(0x07806A50DB893F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (11, 5, N'Master', N'硕士', 3, CAST(0x0700A3455A8A3F400B AS DateTime2), 1, CAST(0x0700FDA75C8A3F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (12, 5, N'Doctor', N'博士', 4, CAST(0x07008FA67B8A3F400B AS DateTime2), 1, CAST(0x078052707D8A3F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (13, 5, N'Other', N'其他', 5, CAST(0x0700D8258B8A3F400B AS DateTime2), 1, CAST(0x07809BEF8C8A3F400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (14, 6, N'Married', N'已婚', 1, CAST(0x07809D83037950400B AS DateTime2), 1, CAST(0x07008E7E067950400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (15, 6, N'UnMarried', N'未婚', 2, CAST(0x0700042F177950400B AS DateTime2), 1, CAST(0x07003160187950400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (16, 6, N'Divorce', N'离异', 3, CAST(0x0700A710297950400B AS DateTime2), 1, CAST(0x07806ADA2A7950400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (17, 6, N'Widowed', N'丧偶', 4, CAST(0x070077233C7950400B AS DateTime2), 1, CAST(0x07803AED3D7950400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (18, 6, N'Other', N'其他', 5, CAST(0x07000CDE467950400B AS DateTime2), 1, CAST(0x0780CFA7487950400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (19, 7, N'Bred', N'未育', 1, CAST(0x07006AAEF97950400B AS DateTime2), 1, CAST(0x07802D78FB7950400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (20, 7, N'UnBred', N'已育', 2, CAST(0x0700C154107A50400B AS DateTime2), 1, CAST(0x0780841E127A50400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (21, 7, N'Other', N'其他', 3, CAST(0x07805431257A50400B AS DateTime2), 1, CAST(0x0780AE93277A50400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (22, 8, N'Han', N'汉族', 1, CAST(0x07002B15727A50400B AS DateTime2), 1, CAST(0x0700B2A8757A50400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (23, 8, N'Other', N'其他', 2, CAST(0x07004763807A50400B AS DateTime2), 1, CAST(0x0780375E837A50400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (24, 9, N'Male', N'男', 1, CAST(0x07002D4CCA7A50400B AS DateTime2), 1, CAST(0x07005A7DCB7A50400B AS DateTime2), 1)
INSERT [dbo].[itemsdetail] ([Id], [ItemId], [ItemCode], [ItemName], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (25, 9, N'Female', N'女', 2, CAST(0x0780DF32D97A50400B AS DateTime2), 1, CAST(0x0700A3FCDA7A50400B AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[itemsdetail] OFF
SET IDENTITY_INSERT [dbo].[logonlog] ON 

INSERT [dbo].[logonlog] ([Id], [LogType], [Account], [RealName], [Description], [IPAddress], [IPAddressName], [CreateTime]) VALUES (131, N'Exit', N'admin', N'zl', N'安全退出系统', N'::1', N'::1', CAST(0x07800096F160B1400B AS DateTime2))
INSERT [dbo].[logonlog] ([Id], [LogType], [Account], [RealName], [Description], [IPAddress], [IPAddressName], [CreateTime]) VALUES (132, N'Login', N'admin', N'zl', N'登陆成功', N'0.0.0.1', N'0.0.0.1', CAST(0x07B052BF2F63B1400B AS DateTime2))
INSERT [dbo].[logonlog] ([Id], [LogType], [Account], [RealName], [Description], [IPAddress], [IPAddressName], [CreateTime]) VALUES (133, N'Login', N'admin', N'zl', N'登陆成功', N'0.0.0.1', N'0.0.0.1', CAST(0x071057109863B1400B AS DateTime2))
SET IDENTITY_INSERT [dbo].[logonlog] OFF
SET IDENTITY_INSERT [dbo].[module] ON 

INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (1, 0, N'系统管理', N'ok-icon', N'&#xe6b0;', N'/', 1, CAST(0x0780BEFFAD8128400B AS DateTime2), 1, CAST(0x0780CC26B58128400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (2, 0, N'系统安全', N'ok-icon', N'&#xe769;', N'/', 2, CAST(0x0700AECF7F8428400B AS DateTime2), 1, CAST(0x07809ECA828428400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (3, 0, N'系统设置', N'ok-icon', N'&#xe889;', N'/', 3, CAST(0x078041AC948428400B AS DateTime2), 1, CAST(0x07809B0E978428400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (4, 1, N'机构管理', N'ok-icon', N'&#xe726;', N'/Permissions/Organize', 1, CAST(0x07806D935A8D28400B AS DateTime2), 1, CAST(0x070079C8955A6B400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (5, 1, N'字典分类', N'ok-icon', N'&#xe640;', N'/Permissions/Items', 2, CAST(0x07002E39E7836C400B AS DateTime2), 1, CAST(0x07002E39E7836C400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (6, 1, N'数据字典', N'ok-icon', N'&#xe660;', N'/Permissions/ItemsDetail', 3, CAST(0x07803B9C468E28400B AS DateTime2), 1, CAST(0x078058075F846C400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (7, 1, N'用户管理', N'ok-icon', N'&#xe736;', N'/Permissions/User', 4, CAST(0x07001305278E28400B AS DateTime2), 1, CAST(0x07006D67298E28400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (8, 1, N'角色管理', N'ok-icon', N'&#xe7be;', N'/Permissions/Role', 5, CAST(0x070003A7C78D28400B AS DateTime2), 1, CAST(0x07005D09CA8D28400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (9, 1, N'系统菜单', N'ok-icon', N'&#xe7ad;', N'/Permissions/Module', 6, CAST(0x07802AB9538E28400B AS DateTime2), 1, CAST(0x0780D056518E28400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (10, 1, N'按钮管理', N'ok-icon', N'&#xe729;', N'/Permissions/Button', 7, CAST(0x07006E0318586C400B AS DateTime2), 1, CAST(0x07006E0318586C400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (11, 2, N'登录日志', N'ok-icon', N'&#xe706;', N'/Security/LogonLog', 1, CAST(0x070004EE46992C400B AS DateTime2), 1, CAST(0x07008B814A992C400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (12, 3, N'网站设置', N'ok-icon', N'&#xe7d5;', N'/SysSet/WebSet', 1, CAST(0x0780832B7D992C400B AS DateTime2), 1, CAST(0x07800ABF80992C400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (13, 3, N'开发设置', N'ok-icon', N'&#xe738;', N'/SysSet/DevSet', 2, CAST(0x07007F919B992C400B AS DateTime2), 1, CAST(0x0700D9F39D992C400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (14, 3, N'邮件设置', N'ok-icon', N'&#xe7bd;', N'/SysSet/EmailSet', 3, CAST(0x0700F541AC992C400B AS DateTime2), 1, CAST(0x07803178AA992C400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (15, 0, N'捐赠管理', N'ok-icon', N'&#xe7d1;', N'/', 0, CAST(0x07007EDAE35470400B AS DateTime2), 1, CAST(0x0700D0BAE75570400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (16, 15, N'捐赠记录', N'ok-icon', N'&#xe644;', N'/Donation/Donation', 1, CAST(0x0780582CE15570400B AS DateTime2), 1, CAST(0x078018764F6070400B AS DateTime2), 1)
INSERT [dbo].[module] ([Id], [ParentId], [FullName], [FontFamily], [Icon], [UrlAddress], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (17, 2, N'定时任务', N'ok-icon', N'&#xe6b2;', N'/Security/Quartz', 2, CAST(0x0700B147DFB8AF400B AS DateTime2), 1, CAST(0x0700B147DFB8AF400B AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[module] OFF
SET IDENTITY_INSERT [dbo].[organize] ON 

INSERT [dbo].[organize] ([Id], [ParentId], [EnCode], [FullName], [CategoryId], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (1, 0, N'Company', N'皮皮虾公司', 1, 1, CAST(0x07801ED7CB952F400B AS DateTime2), 1, CAST(0x0700A48CD9956B400B AS DateTime2), 1)
INSERT [dbo].[organize] ([Id], [ParentId], [EnCode], [FullName], [CategoryId], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (2, 1, N'Market', N'业务部', 2, 5, CAST(0x0780B702438250400B AS DateTime2), 1, CAST(0x078017CC398F6B400B AS DateTime2), 1)
INSERT [dbo].[organize] ([Id], [ParentId], [EnCode], [FullName], [CategoryId], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (3, 1, N'Ministry', N'技术部', 2, 2, CAST(0x0780C5294A8250400B AS DateTime2), 1, CAST(0x07801D631B896B400B AS DateTime2), 1)
INSERT [dbo].[organize] ([Id], [ParentId], [EnCode], [FullName], [CategoryId], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (4, 1, N'HumanResourse', N'人事部', 2, 3, CAST(0x0780FDC5668250400B AS DateTime2), 1, CAST(0x07805728698250400B AS DateTime2), 1)
INSERT [dbo].[organize] ([Id], [ParentId], [EnCode], [FullName], [CategoryId], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (5, 3, N'IT Group', N'研发小组', 3, 4, CAST(0x07008B2AA48250400B AS DateTime2), 1, CAST(0x07806397FC556C400B AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[organize] OFF
SET IDENTITY_INSERT [dbo].[role] ON 

INSERT [dbo].[role] ([Id], [EnCode], [FullName], [TypeClass], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (1, N'administrator', N'超级管理员', 4, 1, CAST(0x07802E81669150400B AS DateTime2), 1, CAST(0x07800150659150400B AS DateTime2), 1)
INSERT [dbo].[role] ([Id], [EnCode], [FullName], [TypeClass], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (2, N'system', N'系统管理员', 4, 2, CAST(0x07801C19E09150400B AS DateTime2), 1, CAST(0x07000D14E39150400B AS DateTime2), 1)
INSERT [dbo].[role] ([Id], [EnCode], [FullName], [TypeClass], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (3, N'user', N'普通用户', 5, 3, CAST(0x07006F25159250400B AS DateTime2), 1, CAST(0x07009C56169250400B AS DateTime2), 1)
INSERT [dbo].[role] ([Id], [EnCode], [FullName], [TypeClass], [SortCode], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (4, N'test', N'测试用户', 6, 4, CAST(0x0780210C249250400B AS DateTime2), 1, CAST(0x0700E5D5259250400B AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[role] OFF
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'1         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'10        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'10        ', N'1         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'10        ', N'2         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'10        ', N'3         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'11        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'11        ', N'2         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'11        ', N'4         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'12        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'13        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'14        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'15        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'16        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'16        ', N'1         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'16        ', N'2         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'16        ', N'3         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'17        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'17        ', N'1         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'17        ', N'2         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'17        ', N'4         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'2         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'3         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'4         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'4         ', N'1         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'4         ', N'2         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'4         ', N'3         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'5         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'5         ', N'1         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'5         ', N'2         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'5         ', N'3         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'6         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'6         ', N'1         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'6         ', N'2         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'6         ', N'3         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'7         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'7         ', N'1         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'7         ', N'2         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'7         ', N'3         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'7         ', N'4         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'7         ', N'5         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'8         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'8         ', N'1         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'8         ', N'2         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'8         ', N'3         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'8         ', N'6         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'9         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'9         ', N'1         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'9         ', N'2         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'1         ', N'9         ', N'3         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'1         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'10        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'11        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'14        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'2         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'3         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'4         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'5         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'6         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'7         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'8         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'3         ', N'9         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'1         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'10        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'11        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'12        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'13        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'14        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'15        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'15        ', N'1         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'16        ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'2         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'3         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'4         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'5         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'6         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'7         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'8         ', N'0         ')
INSERT [dbo].[roleauthorize] ([RoleId], [ModuleId], [ButtonId]) VALUES (N'5         ', N'9         ', N'0         ')
SET IDENTITY_INSERT [dbo].[taskschedule] ON 

INSERT [dbo].[taskschedule] ([Id], [FileName], [JobGroup], [JobName], [CronExpress], [StarRunTime], [EndRunTime], [CreateTime], [State]) VALUES (1, N'Zl.Test.QuartzJob1.dll', N'Zl.Test.QuartzJob1', N'ExampleJob', N'*/10 * * * * ?', CAST(0x070000000000AF400B AS DateTime2), CAST(0x070000000000CA400B AS DateTime2), CAST(0x0700184A1E61B0400B AS DateTime2), 0)
INSERT [dbo].[taskschedule] ([Id], [FileName], [JobGroup], [JobName], [CronExpress], [StarRunTime], [EndRunTime], [CreateTime], [State]) VALUES (2, N'Zl.Test.QuartzJob2.dll', N'Zl.Test.QuartzJob2', N'ExampleJob', N'*/5 * * * * ?', CAST(0x070000000000AF400B AS DateTime2), CAST(0x070000000000C9400B AS DateTime2), CAST(0x0780B4872762B0400B AS DateTime2), 0)
SET IDENTITY_INSERT [dbo].[taskschedule] OFF
SET IDENTITY_INSERT [dbo].[users] ON 

INSERT [dbo].[users] ([Id], [Account], [UserPassword], [RealName], [HeadIcon], [Gender], [Birthday], [MobilePhone], [Email], [WeChat], [DepartmentId], [RoleId], [EnabledMark], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (1, N'admin', N'e10adc3949ba59abbe56e057f20f883e', N'zl', N'/Upload/202001111112451856.png', 1, CAST(0x07000000000061400B AS DateTime2), N'admin', N'12', N'123456', 3, 1, 0, CAST(0x07005184608350400B AS DateTime2), 1, CAST(0x0780162B58819A400B AS DateTime2), 1)
INSERT [dbo].[users] ([Id], [Account], [UserPassword], [RealName], [HeadIcon], [Gender], [Birthday], [MobilePhone], [Email], [WeChat], [DepartmentId], [RoleId], [EnabledMark], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (2, N'user', N'e10adc3949ba59abbe56e057f20f883e', N'user', N'/images/head.jpg', 1, CAST(0x070042B9BB8962400B AS DateTime2), NULL, NULL, NULL, 3, 3, 0, CAST(0x078053D9C27B5F400B AS DateTime2), 1, CAST(0x0780AD3BC57B5F400B AS DateTime2), 1)
INSERT [dbo].[users] ([Id], [Account], [UserPassword], [RealName], [HeadIcon], [Gender], [Birthday], [MobilePhone], [Email], [WeChat], [DepartmentId], [RoleId], [EnabledMark], [CreateTime], [CreateUserId], [UpdateTime], [UpdateUserId]) VALUES (3, N'test', N'e10adc3949ba59abbe56e057f20f883e', N'test', N'/images/head.jpg', 1, CAST(0x07000000000060400B AS DateTime2), NULL, NULL, NULL, 3, 4, 0, CAST(0x07001F41118960400B AS DateTime2), 1, CAST(0x07809577B58A61400B AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[users] OFF
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'EnCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'FullName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'位置' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'Location'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'按钮样式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'ClassName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图标' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'Icon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'SortCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'CreateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button', @level2type=N'COLUMN',@level2name=N'UpdateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'按钮表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'button'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'donation', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'捐赠人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'donation', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'捐赠金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'donation', @level2type=N'COLUMN',@level2name=N'Price'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'捐赠方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'donation', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'donation', @level2type=N'COLUMN',@level2name=N'Detail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'donation', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'捐赠表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'donation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items', @level2type=N'COLUMN',@level2name=N'ParentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items', @level2type=N'COLUMN',@level2name=N'EnCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items', @level2type=N'COLUMN',@level2name=N'FullName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items', @level2type=N'COLUMN',@level2name=N'SortCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items', @level2type=N'COLUMN',@level2name=N'CreateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items', @level2type=N'COLUMN',@level2name=N'UpdateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemsdetail', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemsdetail', @level2type=N'COLUMN',@level2name=N'ItemId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemsdetail', @level2type=N'COLUMN',@level2name=N'ItemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemsdetail', @level2type=N'COLUMN',@level2name=N'ItemName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemsdetail', @level2type=N'COLUMN',@level2name=N'SortCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemsdetail', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemsdetail', @level2type=N'COLUMN',@level2name=N'CreateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemsdetail', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemsdetail', @level2type=N'COLUMN',@level2name=N'UpdateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典明细表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'itemsdetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'logonlog', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'logonlog', @level2type=N'COLUMN',@level2name=N'LogType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'logonlog', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'logonlog', @level2type=N'COLUMN',@level2name=N'RealName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'logonlog', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IP地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'logonlog', @level2type=N'COLUMN',@level2name=N'IPAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IP所在城市' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'logonlog', @level2type=N'COLUMN',@level2name=N'IPAddressName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'logonlog', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录日志表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'logonlog'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'ParentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'FullName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字体类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'FontFamily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图标' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'Icon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'链接' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'UrlAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'SortCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'CreateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module', @level2type=N'COLUMN',@level2name=N'UpdateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'module'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize', @level2type=N'COLUMN',@level2name=N'ParentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize', @level2type=N'COLUMN',@level2name=N'EnCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize', @level2type=N'COLUMN',@level2name=N'FullName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分类' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize', @level2type=N'COLUMN',@level2name=N'CategoryId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize', @level2type=N'COLUMN',@level2name=N'SortCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize', @level2type=N'COLUMN',@level2name=N'CreateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize', @level2type=N'COLUMN',@level2name=N'UpdateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组织表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'organize'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role', @level2type=N'COLUMN',@level2name=N'EnCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role', @level2type=N'COLUMN',@level2name=N'FullName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role', @level2type=N'COLUMN',@level2name=N'TypeClass'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role', @level2type=N'COLUMN',@level2name=N'SortCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role', @level2type=N'COLUMN',@level2name=N'CreateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role', @level2type=N'COLUMN',@level2name=N'UpdateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'roleauthorize', @level2type=N'COLUMN',@level2name=N'RoleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'roleauthorize', @level2type=N'COLUMN',@level2name=N'ModuleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'按钮主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'roleauthorize', @level2type=N'COLUMN',@level2name=N'ButtonId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色授权表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'roleauthorize'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务组' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'taskschedule', @level2type=N'COLUMN',@level2name=N'JobGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'taskschedule', @level2type=N'COLUMN',@level2name=N'JobName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cron表达式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'taskschedule', @level2type=N'COLUMN',@level2name=N'CronExpress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'启动时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'taskschedule', @level2type=N'COLUMN',@level2name=N'StarRunTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'停止时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'taskschedule', @level2type=N'COLUMN',@level2name=N'EndRunTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'taskschedule', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0停止,1启动,2假删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'taskschedule', @level2type=N'COLUMN',@level2name=N'State'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'UserPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'RealName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'头像' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'HeadIcon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'性别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'Gender'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'Birthday'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'MobilePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'微信' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'WeChat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'DepartmentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'RoleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效标志' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'EnabledMark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'CreateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users', @level2type=N'COLUMN',@level2name=N'UpdateUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users'
GO
USE [master]
GO
ALTER DATABASE [zlweb] SET  READ_WRITE 
GO
