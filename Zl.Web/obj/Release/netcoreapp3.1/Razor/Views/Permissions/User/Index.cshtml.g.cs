#pragma checksum "C:\代码\Zl.WebApp\Zl.Web\Views\Permissions\User\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a53b21452c04ec34a1a3308f26d98f710de1b71b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Permissions_User_Index), @"mvc.1.0.view", @"/Views/Permissions/User/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a53b21452c04ec34a1a3308f26d98f710de1b71b", @"/Views/Permissions/User/Index.cshtml")]
    public class Views_Permissions_User_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\代码\Zl.WebApp\Zl.Web\Views\Permissions\User\Index.cshtml"
  
    ViewBag.Title = "Index";
    Layout = "~/Views/Shared/_LayoutList.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<!--模糊搜索区域-->
<div class=""layui-row"">
    <form class=""layui-form layui-col-md12 ok-search"">
        <div class=""layui-input-inline"">
            <input class=""layui-input"" placeholder=""请输入账户"" name=""Account"" autocomplete=""off"">
        </div>
        <div class=""layui-input-inline"">
            <input class=""layui-input"" placeholder=""请输入姓名"" name=""RealName"" autocomplete=""off"">
        </div>
        ");
#nullable restore
#line 16 "C:\代码\Zl.WebApp\Zl.Web\Views\Permissions\User\Index.cshtml"
   Write(Html.EnabledMarkSelectHtml());

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        <div class=\"layui-input-inline\">\r\n            <input class=\"layui-input\" placeholder=\"日期范围\" autocomplete=\"off\" name=\"StartEndDate\" id=\"StartEndDate\">\r\n        </div>\r\n        ");
#nullable restore
#line 20 "C:\代码\Zl.WebApp\Zl.Web\Views\Permissions\User\Index.cshtml"
   Write(Html.SearchBtnHtml("查询"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 21 "C:\代码\Zl.WebApp\Zl.Web\Views\Permissions\User\Index.cshtml"
   Write(Html.ResetBtnHtml());

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
    </form>
</div>
<!--数据表格-->
<table class=""layui-hide"" id=""tableId"" lay-filter=""tableFilter""></table>
<script>
    layui.use([""table"", ""form"", ""laydate"", ""okLayer"", ""okUtils""], function () {
        let table = layui.table;
        let form = layui.form;
        let laydate = layui.laydate;
        let okLayer = layui.okLayer;
        let okUtils = layui.okUtils;
        laydate.render({
            elem: '#StartEndDate'
            , range: '~'
        });

        let AllTable = table.render({
            elem: ""#tableId"",
            url: ""/Permissions/User/List"",
            limit: 10,
            page: true,
            toolbar: ""#toolbarTpl"",
            size: ""sm"",
            cols: [[
                { type: ""checkbox"" },
                { field: ""Id"", title: ""ID"", width: 80, sort: true },
                { field: ""Account"", title: ""账户"", width: 120 },
                { field: ""RealName"", title: ""姓名"", width: 120 },
                { field: ""Gender"", title: ""性别"", width: 8");
            WriteLiteral(@"0, templet: ""#genderTpl"" },
                { field: ""DepartmentName"", title: ""部门"", width: 120 },
                { field: ""RoleName"", title: ""角色"", width: 120 },
                { field: ""EnabledMark"", title: ""状态"", width: 80, templet: ""#enabledMarkTpl"" },
                { field: ""CreateTime"", title: ""创建时间"", width: 150, templet: '<span>{{showDate(d.CreateTime)}}<span>' },
                { title: ""操作"", width: 230,   hight:24, align: ""center"", fixed: ""right"", templet: ""#operationTpl""}
            ]],
            done: function (res, curr, count) {
                console.log(res, curr, count);
            }
        });

        form.on(""submit(search)"", function (data) {
            AllTable.reload({
                where: data.field,
                page: { curr: 1 }
            });
            return false;
        });

        table.on(""toolbar(tableFilter)"", function (obj) {
            switch (obj.event) {
                case ""add"":
                    add();
                    b");
            WriteLiteral(@"reak;
                case ""batchDel"":
                    batchDel();
                    break;
            }
        });

        table.on(""tool(tableFilter)"", function (obj) {
            let data = obj.data;
            switch (obj.event) {
                case ""edit"":
                    edit(data.Id);//field Id 和 数据库表字段 Id 要一致
                    break;
                case ""del"":
                    del(data.Id);
                    break;
                case ""pwdReset"":
                    pwdReset(data.Id);
                    break;
            }
        });

        function add() {
            okLayer.open(""添加用户"", ""/Permissions/User/Add"", ""100%"", ""100%"", null, null);
        }

        function batchDel() {
            okLayer.confirm(""确定要批量删除吗？"", function (index) {
                layer.close(index);
                let idsStr = okUtils.tableBatchCheck(table);
                if (idsStr) {
                    okUtils.ajax(""/Permissions/User/BatchDel"", ""get"", { ids");
            WriteLiteral(@"Str: idsStr }, true).done(function (response) {
                        okUtils.tableSuccessMsg(response.message);
                    }).fail(function (error) {
                        console.log(error)
                    });
                }
            });
        }

        function edit(id) {
            okLayer.open(""编辑用户"", ""/Permissions/User/Edit/"" + id, ""100%"", ""100%"", null, null);
        }

        function del(id) {
            okLayer.confirm(""确定要删除吗？"", function () {
                okUtils.ajax(""/Permissions/User/Delete"", ""get"", { id: id }, true).done(function (response) {
                    okUtils.tableSuccessMsg(response.message);
                }).fail(function (error) {
                    console.log(error)
                });
            })
        }

        function pwdReset(id) {
            okLayer.confirm(""确定要重置密码吗？"", function () {
                okUtils.ajax(""/Permissions/User/InitPwd"", ""get"", { id: id }, true).done(function (response) {
              ");
            WriteLiteral(@"      okUtils.tableSuccessMsg(response.message);
                }).fail(function (error) {
                    console.log(error)
                });
            })
        }

    })
</script>
<!-- 头工具栏模板 -->
<script type=""text/html"" id=""toolbarTpl"">
    ");
#nullable restore
#line 142 "C:\代码\Zl.WebApp\Zl.Web\Views\Permissions\User\Index.cshtml"
Write(Html.TopToolBarHtml(ViewData["TopButtonList"]));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</script>\r\n<!-- 行工具栏模板 -->\r\n<script type=\"text/html\" id=\"operationTpl\">\r\n    ");
#nullable restore
#line 146 "C:\代码\Zl.WebApp\Zl.Web\Views\Permissions\User\Index.cshtml"
Write(Html.RightToolBarHtml(ViewData["RightButtonList"]));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
</script>
<!-- 启用|停用模板 -->
<script type=""text/html"" id=""enabledMarkTpl"">
    {{#  if(!d.EnabledMark){ }}
    <span class=""layui-btn layui-btn-normal layui-btn-xs"">已启用</span>
    {{#  } else{ }}
    <span class=""layui-btn layui-btn-warm layui-btn-xs"">已停用</span>
    {{#  } }}
</script>
<!-- 性别模板 -->
<script type=""text/html"" id=""genderTpl"">
    {{#  if(d.Gender){ }}
    <span>男</span>
    {{#  } else{ }}
    <span>女</span>
    {{#  } }}
</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
